#ifndef RANDOM_MAPPING_HPP
#define RANDOM_MAPPING_HPP

#include <vector>
#include <cstdint>

/**
 * @brief The random_mapping class
 * shuffle() uses rand(), so the user should seed it with srand()
 */
class random_mapping
{
    std::vector<std::uint8_t> map;
    std::vector<std::uint8_t> imap;

public:
    random_mapping(std::uint8_t size);

    /**
     * @brief Function f
     * @param x
     * @return y
     */
    std::uint8_t f(std::uint8_t x);

    /**
     * @brief Function g - inverse of f
     * @param y
     * @return x
     */
    std::uint8_t g(std::uint8_t y);

    /**
     * @brief Shuffle the map
     */
    void shuffle();
};

#endif // RANDOM_MAPPING_HPP
