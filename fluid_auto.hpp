#ifndef FLUID_AUTO_HPP
#define FLUID_AUTO_HPP

#include <exception>
#include <fluidsynth.h>
#include <string>

class fluid_exception;
class fluid_settings_auto;
class fluid_synth_auto;
class fluid_audio_driver_auto;

class fluid_exception : public std::exception
{
public:
    const char * what() const noexcept override;
};

/**
 * @brief Wrapper around fluid_settings_t
 */
class fluid_settings_auto
{
    friend class fluid_synth_auto;
    friend class fluid_audio_driver_auto;
    fluid_settings_t *ptr;
public:
    fluid_settings_auto();
    fluid_settings_auto(fluid_settings_auto &&rhs) noexcept;
    void operator =(fluid_settings_auto &&rhs) noexcept;
    void setstr(const std::string &name, const std::string &str) const;
    void setstr(const char *name, const char *str) const;
    void setint(const std::string &name, int val) const;
    void setint(const char *name, const int val) const;
    ~fluid_settings_auto();
};

/**
 * @brief Wrapper around fluid_synth_t
 */
class fluid_synth_auto
{
    friend class fluid_audio_driver_auto;
    fluid_synth_t *ptr;
public:
    fluid_synth_auto(fluid_settings_auto &fluid_settings);
    fluid_synth_auto(fluid_synth_auto &&rhs) noexcept;
    void operator =(fluid_synth_auto &&rhs) noexcept;
    ~fluid_synth_auto();
    void noteon(int chan, int key, int vel) const;

    /**
     * @brief noteoff
     * Send NOTE_OFF MIDI command on channel chan.
     * @param chan
     * @param key
     * @return FLUID_FAILED if there was an error or there is no matching voice.
     */
    int noteoff(int chan, int key) const noexcept;

    void all_notes_off(int chan) const;
    void all_sounds_off(int chan) const;
    void program_change(int chan, int program) const;
    void pitch_bend(int chan, int val) const;
    void sfload(const std::string &filename, int reset_presets) const;
    void sfload(const char *filename, int reset_presets) const;
};

/**
 * @brief Wrapper around fluid_audio_driver_t
 */
class fluid_audio_driver_auto
{
    fluid_audio_driver_t *ptr;
public:
    fluid_audio_driver_auto() : ptr(nullptr) {}

    /**
     * @brief fluid_audio_driver_auto
     * Starts rendering audio directly when created,
     * so all settings have to be configured before
     * calling this constructor.
     * @param settings
     * @param synth
     */
    fluid_audio_driver_auto(fluid_settings_auto &settings, fluid_synth_auto &synth);
    fluid_audio_driver_auto(fluid_audio_driver_auto &&rhs) noexcept;
    void operator =(fluid_audio_driver_auto &&rhs) noexcept;
    ~fluid_audio_driver_auto();
};

#endif // FLUID_AUTO_HPP
