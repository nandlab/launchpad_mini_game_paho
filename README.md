# Launchpad Mini Game
A game on the Novation Launchpad Mini like [Simon](https://en.wikipedia.org/wiki/Simon_(game)).

# Requirements
## mqtt_node_paho
This project uses the [mqtt_node_paho](https://gitlab.com/NANDLAB/mqtt_node_paho) library as a git subproject and inherits its dependencies (boost, paho.mqtt.cpp). For information about how to install these dependencies see its readme.

## LaunchpadMiniAsync
You also have to fulfill the [LaunchpadMiniAsync](https://gitlab.com/NANDLAB/LaunchpadMiniAsync) requirements (e.g. RtMidi).

## fluidsynth
It synthesizes the audio with fluidsynth, which can be installed with apt (for Raspberry Pi, see below):
```
sudo apt install libfluidsynth-dev
```
On the Raspberry Pi the fluidsynth package in the apt repository is outdated.
It has libfluidsynth1, but libfluidsynth2 is required. Build and install the latest version as follows:
```
git clone https://github.com/FluidSynth/fluidsynth.git
cd fluidsynth
mkdir build
cd build
cmake ..
make
sudo make install
```

You can go back to the previous directory with `cd ../..`.

## Boost program_options
The boost program_options library is used for parsing the game options from the command line or a config file.
```
sudo apt install libboost-program-options-dev
```

# Building launchpad_mini_game_paho
On Debian-based distros, create and install a deb package as follows.
Set LP_INI_AUDIO_DEV to the audio device to use by default on your system.
On the Raspberry Pi it would be `hw:0,0` for HDMI and `hw:1,0` for the audio jack.
```
git clone --recursive https://gitlab.com/NANDLAB/launchpad_mini_game_paho
cd launchpad_mini_game_paho
mkdir build
cd build
cmake .. -DLP_BUILD_DEB=ON -DLP_INI_AUDIO_DEV=hw:1,0
make
cpack
sudo apt install ./*.deb
```
This installs the following files:
| Type | Path |
|------|------|
| Executable | `/usr/bin/launchpad_mini_game_paho` |
| Config File | `/etc/launchpad_mini_game_paho.ini` |
| Systemd Script | `/usr/lib/systemd/system/launchpad_mini_game_paho.service` |
| White Stripes Song | `/usr/share/launchpad_mini_game_paho/songs/white_stripes.mid` |

(See also the produced `install_manifest.txt` file in the build directory.)

The systemd script launches the game on boot, it is disabled by default. You can enable it with:
```
sudo systemctl enable launchpad_mini_game_paho
```

# Usage
To see the recognized program options type `launchpad_mini_game_paho --help`.
